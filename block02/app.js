const express = require ('express');
const	app = express();
const port = 3003;
const bodyParser = require('body-parser');
//============ initial settings =====================
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.listen (port, ()=>{
    console.log(`server is running on port ${port}`)
});


let DB = [
    {
        category:'laptops', 
        products:[
            {
                name:'Lenovo', 
                price:699.95,
                color: 'purple',
                description: 'Best brand in 2018'
            },
            {
            	name:'Acer', 
                price:499.95,
                color: 'green',
                description: 'Amazing product'
            },
            {
            	name:'Mac', 
                price:1999.95,
                color: 'blue',
                description: 'No laptop should cost as a car'
            }
        ]
    },
    {
        category:'mobiles', 
        products:[
            {
                name:'Samsung', 
                price:499.99,
                color: 'purple',
                description: 'too expensive'
            },
            {
            	name:'iPhone', 
                price:599.99,
                color: 'green',
                description: 'overrated'
            },
            {
            	name:'Huawei', 
                price:299.99,
                color: 'blue',
                description: 'underrated'
            }
        ]
    }

];



// *** ADD NEW CATEGORY *** WORKING!!!!
app.post('/category/add', (req, res)=>{
	let {category} = req.body;
    DB.forEach(function(ele){
	//if (DB.indexOf(category) !== -1) {
        if (ele.category === category) {
		console.log('****',  {category}, '****'); 		
        res.send(`Category ${category} already exists`);
    } else {
        res.send(`New category: "${category}" added`);
    }
    DB.push({category}); 
    });
    res.send(DB);
});


// *** UPDATE A CATEGORY *** not working for doesn't exist, sends the same message as previous when updated succesfully
app.post('/category/update', (req, res)=>{
  let category = req.body.category;
  let newCategory = req.body.newCategory;
      DB.forEach(function(ele){
      if (ele.category !== category) {
        let message = `The category: ${category} doesn't exist.`;
      } else {
        console.log(ele.category);
        ele.category = newCategory;
        message = `Category ${category} updated to ${newCategory}.`;
    }
    //DB.push({category}); 
    res.send({message});
    });
    
});




// *** REMOVE A CATEGORY *** WORKING!!!
app.post('/category/delete', (req, res)=>{
    let {category} = req.body;
    DB.forEach(function(ele){
    if (ele.category === category) {
        //delete ele.category;
        DB.splice(DB.indexOf(ele), 1);
        res.send(`Category ${category} deleted`);
    } else {
        res.send(`Category ${category} does not exist`);
    }
});
    res.send(DB);
});

  


// *** DISPLAY ALL CATEGORIES ***  WORKS!!!!
app.get('/category/categories', (req, res) =>{
   let categories = DB.map((ele)=>
        ele.category);
        res.send({categories});
});



// *** display the categories with all products *** WORKING!!!!
app.get('/category/products', (req, res)=>{
    //console.log(DB);
        res.send(DB);
});


// *****works for laptops not for mobs*****
// *** display one category with all its products ***
app.get ('/category/:category', (req, res)=>{ 
    let category = req.params.category;
    DB.forEach(function(ele){
        if (ele.category !== category) {
          res.send(`Category ${category} does not exist`);
        } else {
          res.send (ele);
        }
    });
});

/*
// POST   | /product/add        | Add product   
*/


//*** Delete product *** NOT WORKING!!!!!!
/*
app.post('/product/delete', (req, res)=>{
    let {product} = req.body;
    DB.forEach(function(ele){
      index = DB.indexOf(ele);
      DB[index].products.forEach(function(product){
        if (product.name === product) {
          DB.splice(DB.indexOf(ele), 1);
          res.send(`Category ${product} deleted`);
        } else {
          res.send(`Category ${product} does not exist`);
        }
      });
    });
    res.send(DB);
});*/
    
    /*//if (ele.products === category) {
        //delete ele.category;
        //DB.splice(DB.indexOf(ele), 1);
        res.send(`Category ${category} deleted`);
    } else {
        res.send(`Category ${category} does not exist`);
    }
});
    res.send(DB);
});
*/


/*
// POST   | /product/update     | Update name, price, color or description
*/


