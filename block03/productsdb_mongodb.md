
Use your ProductsDB_noDB App from block 02 and refactor it 
to connect to and use MongoDB through Mongoose. 
So that you will be able to store your products in the DB
and not loose them after the server is restarted.

EXERCISE
```
//=====>   /categories/add      POST            // add new category to DB
//=====>   /categories/remove   POST            // remove category from DB
//=====>   /categories/update   POST            // update category
//=====>   /categories          GET             // GET all categories
//=====>   /categories/:product GET             // GET one product by name or id 
//=====>   /products/add        POST            // add new product to DB
//=====>   /products/remove     POST            // remove product from DB
//=====>   /products/update     POST            // update product
//=====>   /products            GET             // get all products
//=====>   /products/:product   GET             // get one product by title or id 
//=====>   /products/:category  GET             // get all products that belongs to a specific category
```

***Your solution goes to the current folder***