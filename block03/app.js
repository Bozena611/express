const express = require ('express');
const app = express();
const port = 3001;
const mongoose = require ('mongoose');
const ProductsRoute = require ('./routes/ProductsR');
const bodyParser = require ('body-parser');

// =================== initial settings ===================
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// connnect to mongo

// connecting to mongo and checking if DB is running
async function connecting(){
try {
    await mongoose.connect('mongodb://localhost/block3', { useUnifiedTopology: true , useNewUrlParser: true })
    console.log('Connected to the DB')
} catch ( error ) {
    console.log('ERROR: Seems like your DB is not running, please start it up !!!');
}
}
connecting()
// temp stuff to suppress internal warning of mongoose which would be updated by them soon
mongoose.set('useCreateIndex', true);
// end of connecting to mongo and checking if DB is running

// routes
app.use('/', ProductsRoute); //check if ok





app.listen (port, ()=>{
	console.log(`server is running on port ${port}`)
});