const express = require('express');
const router = express.Router();
const controller = require('../controllers/ProductsController');   
const bodyParser = require ('body-parser');

router.get('/', controller.getProducts);


// add new category to DB
router.post('/categories/add', controller.addCategory);

// remove category from DB
router.post('/categories/remove', controller.removeCategory);

// update category
router.post('/categories/update', controller.updateCategory);

// GET all categories
router.get('/categories', controller.showAllCategories);

// GET one product by name or id
router.get('/categories/:product', controller.findOneProduct);   //same as below???

// add new product to DB
router.post('/products/add', controller.addProduct);

 // remove product from DB
router.post('/products/remove', controller.removeProduct);

// update product
router.post('/products/update', controller.updateProduct);

// get all products
router.get('/products', controller.getProducts);

// get one product by title or id
router.get('/products/:product', controller.getOneProduct);  	//same as above???

// get all products that belong to a specific category
router.get('/products/:category', controller.showAllCategoryProducts); //conflict with products/:product


module.exports = router;