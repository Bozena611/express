const mongoose = require ('mongoose');

const productSchema = new mongoose.Schema ({
    	name: {type: String, required: true, unique: true},
		price: {type: Number, required: true},
		color: {type: String, required: true},
		description: {type: String, required: true, unique: true},
		category_id: {type:mongoose.Schema.Types.ObjectId, required: true}
});

module.exports = mongoose.model('products', productSchema);