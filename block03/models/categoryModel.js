const mongoose = require ('mongoose');

const categorySchema = new mongoose.Schema ({
    category: {
    	type: String, 
    	unique:true, 
    	required: true
    }
});

module.exports = mongoose.model('categories', categorySchema);


/*const ProductsDB = {
    
    laptops: [ 
            {
                name:'Lenovo', 
                price:699.95,
                color: 'purple',
                description: 'Best brand in 2018'
            },
            {
                name:'Acer', 
                price:499.95,
                color: 'green',
                description: 'Amazing product'
            },
            {
                name:'Mac', 
                price:1999.95,
                color: 'blue',
                description: 'No laptop should cost as a car'
            }
        
    ],
    mobile phones: [ 
            {
                name:'Samsung', 
                price:499.99,
                color: 'purple',
                description: 'too expensive'
            },
            {
                name:'iPhone', 
                price:599.99,
                color: 'green',
                description: 'overrated'
            },
            {
                name:'Huawei', 
                price:299.99,
                color: 'blue',
                description: 'underrated'
            }
    ]
    
};*/