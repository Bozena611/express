const Categories = require ('../models/categoryModel');
const Products = require('../models/productModel');
const bodyParser = require ('body-parser');

class ProductsController {


	async getProducts(req, res){
		console.log('ProductsController is working!');
		try{
			const products = await Products.find({});
			res.send({products});
		}
		catch(err) {
			res.send({err});
		};
	};

// add new category to DB
	async addCategory(req, res){
		let { category } = req.body; 
		try{
			const newCategory = await Categories.create({category});
			res.send({newCategory});
		}
		catch(err){
			res.send({err});
		}
	};
// remove category from DB
	async removeCategory(req, res){
		let { _id } = req.body;
		try{
			const removedC = await Categories.remove({_id});
			res.send (removedC); 
		}
		catch(err){
			res.send({err});
		}
	};

// update category
	async updateCategory(req, res){
		let { _id, newCategory } = req.body;
		try{
			const updatedC = await  Categories.update({_id}, {$set:{ category:newCategory }});
			res.send (updatedC);
		}	
		catch(err){
			res.send({err});
		}
	}


// GET all categories
	async showAllCategories(req, res){
		try{
			const categories = await Categories.find({});
			res.send({categories});
		}
		catch(err) {
			res.send({err});
		};
	};

// GET one product by name or id 	****WORKS only with ID****
	async findOneProduct(req, res) {
		//let { product_id, name } = req.params; 
		let {product} = req.params;
		try{
			//const oneProduct = await Products.findOne({$or:[{_id:product_id}, {name: name}]}); 
			const oneProduct = await Products.findOne({$or:[{_id:product}, {name: product}]});
			res.send(oneProduct);
		}
		catch(err){
			res.send({err});
		}
	}


// add new product to DB	
	async addProduct(req, res){
		let { name, price, color, description, category_id } = req.body; 
		try{
			const newProduct = await Products.create({
				name:name, price:price, color:color, description:description, category_id:category_id
			});
			res.send({newProduct});
		}
		catch(err){
			res.send({err});
		}
	};


// remove product from DB
	async removeProduct(req,res) {
		let { _id } = req.body;
		try{
			const removed = await Products.remove({_id}); 
			res.send (removed); 
		}
		catch(err){
			res.send({err});
		}
	};

// update product
	async updateProduct(req, res){
		let { _id, newName, newPrice, newColor, newDescription, category_id } = req.body;
		try{
			const updatedP = await Products.update({_id}, {$set:{ name: newName, price:newPrice, color:newColor, description:newDescription, category_id:category_id }});
			res.send (updatedP);
		}	
		catch(err){
			res.send({err});
		}
	}

// get all products
	async getProducts(req, res){
		try{
			const products = await Products.find({});
			res.send({products});
		}
		catch(err) {
			res.send({err});
		};
	};


// get one product by title or id ****WORKS only with ID****

	async getOneProduct(req, res) {		
		//let { product_id, name } = req.params; 
		let { product } = req.params;
		try{
			//const oneProduct = await Products.findOne({$or:[{_id:5dcea341b67b0c1ba0da7afc}, {name: req.params}]}); 
			const oneProduct = await Products.findOne({$or:[{_id:product}, {name:product}]});
			res.send(oneProduct);
		}
		catch(err){
			res.send({err});
		};
	};

//******NOT WORKING!!!*****

// get all products that belong to a specific category 
//wordsModel.find({language_id: 'some language id from the language schema goes here'})
//router.get('/products/:category', controller.showAllCategoryProducts)

	async showAllCategoryProducts(req, res){
		let { category } = req.params;
		try{
			const Cproducts = await Products.find({category_id:category});
			res.send (Cproducts); 
		}
		catch(err){
			res.send({err});
		}
	};
	

//closes the class ProductsController
}

module.exports = new ProductsController();