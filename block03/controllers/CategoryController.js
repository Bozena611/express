const Categories = require('../models/categoryModel');

class CategoryController {

	async getProducts(req, res){
		console.log('ProductsController is working!');
		try{
			const products = await Products.find({});
			res.send({products});
		}
		catch(err) {
			res.send({err});
		};
	};

// add new category to DB
	async addCategory(req, res){
		let { category } = req.body; 
		try{
			const newCategory = await Categories.create({
				category: category
			});
			res.send({newCategory});
		}
		catch(err){
			res.send({err});
		}
	};
// remove category from DB
	async removeCategory(req, res){
		let { _id } = req.body;
		try{
			const removedC = await Categories.remove({_id});
			res.send (removedC); 
		}
		catch(err){
			res.send({err});
		}
	};

// update category
	async updateCategory(req, res){
		let { _id, newCategory } = req.body;
		try{
			const updatedC = await  Categories.update({_id}, {$set:{ category:newCategory }});
			res.send (updatedC);
		}	
		catch(err){
			res.send({err});
		}
	}


// GET all categories
	async showAllCategories(req, res){
		try{
			const categories = await Categories.find({});
			res.send({categories});
		}
		catch(err) {
			res.send({err});
		};
	};


//closes the class ProductsController
}

module.exports = new ProductsController();