const express = require('express');
const app = express();
const port = 3001;

app.listen (port, () => {
	console.log(`server is running on ${port}`)
});

let list = {
		NL: 'Hallo Wereld',
	    HI: 'नमस्ते दुनिया',
	    FR: 'Bonjour le monde',
	    ES: 'Hola Mundo',
	    IT: 'Ciao Mondo',
	    CH: '你好，世界',
	    JP: 'こんにちは世界',
	    AR: 'رحبا بالعالم',
	    EN:'Hello world'
	};

	
app.get('/:language', (req, res)=> {
	var message = list [req.params.language] || list.EN;
	res.send (`<h1>${message}</h1>`);
});

app.get('/', ( req, res)=>{
	res.send('<h1>Hello world</h1>');
})

app.get('*', (req, res)=>{
	res.send('Hello world');
});