const express = require('express');
const app = express();
const port = 3001;

app.listen (port, () => {
	console.log(`server is running on ${port}`)
});

let list = {
		NL: 'Hallo Wereld',
	    HI: 'नमस्ते दुनिया',
	    FR: 'Bonjour le monde',
	    ES: 'Hola Mundo',
	    IT: 'Ciao Mondo',
	    CH: '你好，世界',
	    JP: 'こんにちは世界',
	    AR: 'رحبا بالعالم',
	    EN:'Hello world'
	};

app.get('/:language', (req, res) =>{
	var data = req.params.language;
	for (var language in list) {
		if (data == language){
			var message = list[language]
		} else if (!(data in list)) {
			message = `The phrase <em>Hello World</em> in ${data} not found, please input /:newLang/:phrase.`
		}
	}
	res.send(`<h2>${message}</h2>`)
});


app.get ('/:newLang/:phrase', (req, res) => {
	var newLang = req.params.newLang;
	var phrase = req.params.phrase.split('_').join(' ');
	if (!(newLang in list)) {
		list[newLang] = phrase;    
		res.send(`<em>${req.params.phrase}</em>, ${newLang} added with message "${phrase}"`) 
	}
});

app.get('/', ( req, res)=>{
	res.send('<h1>Hello world</h1>');
});

app.get('*', (req, res)=>{
	res.send('Hello world');
});