const express = require ('express');
const app = express();
const port = 3001;

app.listen (port, () =>{
	console.log(`server is running on port ${port}`)
});


let list = {};

app.get('/:language', (req, res) =>{
	var data = req.params.language;
	if (!(data in list)) {
			var message = `The phrase <em>Hello World</em> in ${data} not found, please input: /:newLang/:phrase.`
		} else if (data in list){
			 message = list[req.params.language];
		}  
	res.send(`<h2>${message}</h2>`)
});




// *** REMOVE ***
app.get ('/:language/remove', (req, res)=>{			
	const { language } = req.params;				
	delete list[language];
	res.send(`<h1> ${language} removed</h1>`)
});

// *** UPDATE ***
app.get('/:language/update/:newPhrase', (req, res)=> {
	var language = req.params.language;
	var newPhrase = req.params.newPhrase;
	var phrase = list[language];
	list[language] = newPhrase.replace("_", " ");

	res.send(`<h1> ${language} updated from "${phrase}"" to "${req.params.newPhrase}" </h1>`)
})

// **** ADD A NEW LANGUAGE ****
app.get ('/:newLang/:phrase', (req, res) => {
	var newLang = req.params.newLang;
	var phrase = req.params.phrase.split('_').join(' ');
	if (!(newLang in list)) {
		list[newLang] = phrase;  
		console.log(list);  
		res.send(`<h2><em>${req.params.phrase}</em>, ${newLang} added with message "${phrase}"</h2>`) 
	} else if (newLang in list) {
		res.send(`<h2>Action fobidden, ${newLang} is already present in the system, if you wish to update please enter new phrase using: /:language/update/:newPhrase </h2>`)
	}
});

app.get('/', (req, res)=>{
	res.send('<h1>Hello world</h1>');
})

app.get('*', (req, res)=>{
	res.send('<h1>Hello world</h1>');
});