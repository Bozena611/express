const express = require ('express');
const app = express();
const port = 3001;

app.listen (port, () =>{
	console.log(`server is running on port ${port}`)
});

let list = {};

app.get('/:language', (req, res) =>{
	var data = req.params.language;
	if (!(data in list)) {
			message = `The phrase <em>Hello World</em> in ${data} not found, please input /:newLang/:phrase.`
		} else if (data in list){
			var message = list[req.params.language];
		}  
	res.send(`<h2>${message}</h2>`)
});


// *** REMOVE 1 ***
app.get ('/:language/remove', (req, res)=>{			// moved in front of add new language because it triggers
	const { language } = req.params;				// the add option due to the same route (changed route below)
	delete list[language];
	res.send(`<h1> ${language} removed</h1>`)
});


// **** ADD A NEW LANGUAGE ****
app.get ('/:newLang/:phrase', (req, res) => {		//changed route to avoid similarities with remove
	var newLang = req.params.newLang;
	var phrase = req.params.phrase.split('_').join(' ');
	if (!(newLang in list)) {
		list[newLang] = phrase;  
		console.log(list);  
		res.send(`<h2><em>${req.params.phrase}</em>, ${newLang} added with message "${phrase}"</h2>`) 
	}
});



// *** REMOVE 2 ***
/*app.get ('/home/:language/remove', (req, res)=>{		// even if I change route it still adds new new language 
	const { language } = req.params;				// message EN added with message EN for /EN/remove
	delete list[language];
	res.send(`<h1> ${language} removed</h1>`)
});*/

app.get('/', (req, res)=>{
	res.send('<h1>Hello world</h1>');
});

app.get('*', (req, res)=>{
	res.send('<h1>Hello world</h1>');
});