

app.get('/:language', (req, res) =>{
	var data = req.params.language;
	res.send(`The phrase <em>Hello World</em> in ${data} not found, please input /:newLang/:phrase.`);
});
